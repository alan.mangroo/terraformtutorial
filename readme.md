# Terraform Tutorial

## Credentials
Credentials stored in aws-vault
https://github.com/99designs/aws-vault

```aws-vault add tfuser```

Manually add MFA config and region:
```
[profile tfuser]
region=ap-southeast-2
mfa_serial=arn:aws:iam::XXXXXXXXX:mfa/tfuser
```

```aws-vault list```


```aws-vault exec tfuser --duration=12h```

## Running Terraform
First time

```docker-compose run --rm tf init```


Format Terraform file

```docker-compose run --rm tf fmt```


Validate Terraform file

```docker-compose run --rm tf validate```


Plan the changes

```docker-compose run --rm tf plan```


Apply changes

```docker-compose run --rm tf apply```

Destroy infrastructure

```docker-compose run --rm tf destroy```