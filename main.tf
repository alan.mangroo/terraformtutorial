provider "aws" {
  version = "~> 2.70.0"
  region  = "ap-southeast-2"
}

resource "aws_instance" "webserver" {
  ami           = "ami-0ded330691a314693"
  instance_type = "t2.micro"
  key_name      = "tfuser"

  security_groups = [aws_security_group.webserversg.name]

  tags = {
    Name = "MyWebServer"
  }
}

resource "aws_security_group" "webserversg" {
  name        = "MyWebSG"
  description = "A web SG"
  ingress {
    description = "Allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "instance_public_dns" {
  value = aws_instance.webserver.public_dns
}
